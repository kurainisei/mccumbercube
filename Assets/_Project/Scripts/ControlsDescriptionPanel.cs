﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ControlsDescriptionPanel : MonoBehaviour
{
    public TextMeshProUGUI titleLabel;
    public TextMeshProUGUI descriptionLabel;
    public TextMeshProUGUI referencesLabel;
    public Color normalColor=Color.red;
    public Color activeColor=Color.cyan;
    private CanvasGroup _canvas;
    private Image _background;
    // Start is called before the first frame update
    void Start()
    {
        _canvas = GetComponent<CanvasGroup>();
        _background = GetComponent<Image>();
        _canvas.alpha = 0;
    }

    public void Appear(bool isCompleted=false, string title="", string description="", string references="")
    {
        if (isCompleted)
        {
            _background.color = activeColor;
        }
        else
        {
            _background.color = normalColor;
        }

        titleLabel.text = title;
        descriptionLabel.text = description;
        referencesLabel.text = references;

        _canvas.DOFade(1, .5f).SetEase(Ease.InOutQuad);
    }

    public void Disappear()
    {
        _canvas.DOFade(0, .5f).SetEase(Ease.InOutQuad);
    }
}

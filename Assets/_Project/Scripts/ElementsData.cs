﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Element
{
    public string elementName;
    [TextArea]
    public string elementDescription;
}
[System.Serializable]
public class Control
{
    public string name;
    [TextArea]
    public string description;
    [TextArea]
    public string confirmation;
    public int[] activators;
    public string[] refLabels;
}
[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Elements Data", order = 1)] 
public class ElementsData : ScriptableObject
{
    [SerializeField]
    public List<Element> elements;

    public List<Control> controls;
    
    public bool[] currentCubeStatus;
    //this is for checking cubes that have two activators
    //Place 0 refers to cube 9
    //place 1 refers to cube 13
    public bool[] doubleCubesStatus;
    
    public int[] ActiveCubeIDs()
    {
        List<int> tempCubeIds=new List<int>();
        for (int i = 0; i < currentCubeStatus.Length; i++)
        {
            if (currentCubeStatus[i] == true)
            {
                tempCubeIds.Add(i);
            }
        }

        return tempCubeIds.ToArray();
    }

    public int[] cubeControls;
    
    private void OnDisable()
    {
        for (int i = 0; i < currentCubeStatus.Length; i++)
        {
            currentCubeStatus[i] = false;
        }
        
        for (int i = 0; i < doubleCubesStatus.Length; i++)
        {
            doubleCubesStatus[i] = false;
        }
    }
}


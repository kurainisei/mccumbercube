﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;


public class DragInterfaceControl : MonoBehaviour
{
    public CanvasGroup dashboard;
    public CanvasGroup explanation;
    public ControlsDescriptionPanel descriptionPanel;
    public ElementsData data;
    public CubeArray cubes;
    public bool canHover;
    private void OnEnable()
    {   
        cubes.OnRightCubeHover.AddListener(CubeHover);
        cubes.OnEndHover.AddListener(CubeEndHover);
        explanation.alpha = 1;
        Sequence opening = DOTween.Sequence();
        opening
            .AppendInterval(6)
            .AppendCallback(OpenDashboard)
            .Append(explanation.DOFade(0, 1).SetEase(Ease.InOutQuad));
        canHover = true;
    }

    public void OnDisable()
    {
        cubes.OnRightCubeHover.RemoveListener(CubeHover);
    }

    void OpenDashboard()
    {
        dashboard.DOFade(1, 1).SetEase(Ease.InOutQuad).OnComplete(EnableDashboard);
    }

    void EnableDashboard()
    {
        dashboard.interactable = true;
    }
    private void Start()
    {
        dashboard.alpha = 0;
        //explanation.alpha = 0;
    }

    void CubeHover(int i)
    {
        if (canHover)
        {
            //find the activator data linked to the hovered cube
            int activatorID = data.cubeControls[i];
        
            //patch to deal with double cubes)
            if (i == 9&&!data.doubleCubesStatus[0])
            {
                activatorID = 11;
            }
            else if (i == 13 && !data.doubleCubesStatus[1])
            {
                activatorID = 6;
            }
        
            string controlName = data.controls[activatorID].name;
            string description = data.controls[activatorID].description;
            string references="";
        
            foreach (var reference in data.controls[activatorID].refLabels)
            {
                references += reference + ", ";
            }

            char[] c = {' ', ',' };
            references = references.Trim(c);
        
            descriptionPanel.Appear(true, controlName, description, references);
        }
        
    }

    void CubeEndHover()
    {
        if (canHover)
            descriptionPanel.Disappear();
    }
    
}

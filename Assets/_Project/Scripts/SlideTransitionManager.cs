﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Playables;

public class SlideTransitionManager : MonoBehaviour
{
    public CanvasGroup fadingSlide1;
    public CanvasGroup fadingSlide2;
    public Canvas slide1;
    public Canvas slide2;
    public Transform slide2Labels;
    //temporary solution to be changed
    public GameObject draggable;
    public CubeArray cube;
    public CinemachineVirtualCamera[] cameras;
    public GameObject cubeInteractables;
    public PlayableAsset cubeExplode;
    public PlayableAsset cubeImplode;
    public ElementsData data;
    private PlayableDirector _director;
    
    // Start is called before the first frame update
    void Start()
    {
        _director = GetComponent<PlayableDirector>();
        fadingSlide1.alpha = 0;
        fadingSlide2.alpha = 0;
        slide1.enabled = false;
        slide2.enabled = false;
        cubeInteractables.SetActive(false);
        cameras[0].enabled=true;
        cameras[1].enabled = false;
        for (int i=0; i<data.currentCubeStatus.Length; i++)
        {
            data.currentCubeStatus[i] = false;
        }
        BeginGame();
    }

    void BeginGame()
    {
        slide1.enabled = true;
        fadingSlide1.DOFade(1, .5f).SetEase(Ease.InOutQuad);
        
    }

    public void Toslide2()
    {
        fadingSlide1.DOFade(0, 1).SetEase(Ease.InOutQuad).OnComplete(StartSlide2);
    }

    public void StartSlide2()
    {
        slide1.enabled = false;
        slide2.enabled = true;
        slide2Labels.gameObject.SetActive(true);
        cubeInteractables.SetActive(true);
        fadingSlide2.DOFade(1, 1).SetEase(Ease.InOutQuad);
        cameras[0].enabled = false;
        cameras[1].enabled = true;
    }

    public void ToMainInteraction()
    {
        cube.ResetAll();
        fadingSlide2.DOFade(0, 1).SetEase(Ease.InOutQuad).OnComplete(StartMainInteraction);
    }

    void StartMainInteraction()
    {
        slide2.enabled = false;
        slide2Labels.gameObject.SetActive(false);
        
        cube.AllRightAnswers(data.ActiveCubeIDs());
        _director.playableAsset = cubeExplode;
        _director.Play();
        draggable.SetActive(true);
    }

    public void BackToSlide2()
    {
        cube.ResetAll();
        draggable.SetActive(false);
        _director.playableAsset = cubeImplode;
        _director.Play();
    }
}

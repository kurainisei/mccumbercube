﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DescriptionPanel : MonoBehaviour
{
    public TextMeshProUGUI titleLabel;
    public TextMeshProUGUI textLabel;
    public ElementsData data;
    private CanvasGroup _group;
    
    // Start is called before the first frame update
    void Start()
    {
        _group = GetComponent<CanvasGroup>();
        _group.alpha = 0;
    }

    public void DisplayInfo(int buttonClicked)
    {
        _group.alpha = 1;
        titleLabel.text = data.elements[buttonClicked].elementName;
        textLabel.text = data.elements[buttonClicked].elementDescription;
    }
}

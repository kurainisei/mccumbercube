﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CategoryButton : MonoBehaviour
{
    public DescriptionPanel descriptionPanel;
    public ElementsData data;
    public int buttonID;
    private TextMeshProUGUI _label;
    // Start is called before the first frame update
    void Start()
    {
        _label = GetComponentInChildren<TextMeshProUGUI>();
        _label.text = data.elements[buttonID].elementName;
    }

    public void ButtonClicked()
    {
        descriptionPanel.DisplayInfo(buttonID);
    }
}

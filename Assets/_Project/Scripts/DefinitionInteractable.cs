﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DefinitionInteractable : MonoBehaviour
{
    public TextMeshPro label;
    public ElementsData data;
    public int interactableId;
    public DescriptionPanel descriptionPanel;
    public CubeInteractions[] controlledCubes;
    public CubeArray allCubes;
    // Start is called before the first frame update
    void Start()
    {
        label.text = data.elements[interactableId].elementName;
    }

    private void OnMouseEnter()
    {
        foreach (CubeInteractions c in controlledCubes)
        {
            c.ToggleColor();
        }
    }
    
    private void OnMouseExit()
    {
        foreach (CubeInteractions c in controlledCubes)
        {
            c.ToggleColor();
        }
    }

    private void OnMouseDown()
    {
        foreach (CubeInteractions c in allCubes.cubes)
        {
            c.Deselect();
        }
        
        descriptionPanel.DisplayInfo(interactableId);

        foreach (CubeInteractions c in controlledCubes)
        {
            c.Select();
        }
    }
}

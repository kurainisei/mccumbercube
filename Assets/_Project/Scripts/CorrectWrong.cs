﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CorrectWrong : MonoBehaviour
{
    public Sprite correctIcon;
    public Sprite wrongIcon;
    public Color correctColor = Color.green;
    public Color wrongColor = Color.red;
    public AudioClip correctSound;
    public AudioClip wrongSound;
    public ElementsData data;
    
    public Image responseIcon;
    public CanvasGroup responsePanelGroup;
    public Image responsePanelGraphic;
    public TextMeshProUGUI title;
    public TextMeshProUGUI response;
    private bool _isShowing;
    private Vector2 initialScale;
    private AudioSource _audio;
    // Start is called before the first frame update
    void Start()
    {
        responsePanelGroup.alpha = 0;
        _isShowing = false;
        _audio = GetComponent<AudioSource>();
    }

    public void OnDisable()
    {
        StopAllCoroutines();
        responsePanelGroup.alpha = 0;
    }

    public void ShowAnswer(bool isCorrect, int answerID)
    {
        StopAllCoroutines();
        //setup
        if (isCorrect)
        {
            responsePanelGraphic.color = correctColor;
            responseIcon.sprite = correctIcon;
            title.text = "correct";
            //_audio.PlayOneShot(correctSound);
            //patch for dealing with double cubes. I keep track if a certain double cube has been selected
            if (answerID == 9)
            {
                data.doubleCubesStatus[0] = true;
            }
            else if (answerID == 1)
            {
                data.doubleCubesStatus[1] = true;
            }
        }

        else
        {
            responsePanelGraphic.color = wrongColor;
            responseIcon.sprite = wrongIcon;
            title.text = "incorrect";
            //_audio.PlayOneShot(wrongSound);
        }

        response.text = data.controls[answerID].confirmation;

        responsePanelGroup.DOFade(1,1).SetEase(Ease.InOutQuad);
        _isShowing = true;
        StartCoroutine(ScheduledDisappear());
    }

    IEnumerator ScheduledDisappear()
    {
        yield return new WaitForSeconds(5);
        Disappear();
    }
    
    public void Disappear()
    {
        if (_isShowing)
        {
            StopCoroutine(ScheduledDisappear());
            responsePanelGroup.DOFade(0,.5f).SetEase(Ease.InOutQuad);
            _isShowing = false;
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragNDrop : MonoBehaviour, IBeginDragHandler, IDragHandler, IDropHandler, IPointerEnterHandler, IPointerExitHandler, IEndDragHandler
{
    public Canvas canvas;
    public ElementsData data;
    public int controlID;
    public CubeArray allCubes;
    public Color completedColor = Color.green;
    
    //this is a bit sloppy, might change late
    public ControlsDescriptionPanel descriptionPanel;
    

    public CorrectWrong feedbackDisplay;
    private DragInterfaceControl _dControl;
    private IDragHandler _dragHandlerImplementation;
    private RectTransform _rect;
    private Image _panelImage;
    private TextMeshProUGUI _label;
    private Vector2 _startRectPosition;
    private bool _canHover = true;
    private bool _isDraggable = true;

    void Awake()
    {
        _rect = GetComponent<RectTransform>();
        _label = GetComponentInChildren<TextMeshProUGUI>();
        _label.text = data.controls[controlID].name;
        _startRectPosition = _rect.anchoredPosition;
        _panelImage = GetComponent<Image>();
        _isDraggable = true;
        _dControl = GetComponentInParent<DragInterfaceControl>();
        //Debug.Log(_startRectPosition);
    }
    

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (_isDraggable)
        {
            _rect.anchoredPosition += eventData.delta/canvas.scaleFactor;
            StopAllCoroutines();
            feedbackDisplay.Disappear();
        }
        
    }

    public void OnDrop(PointerEventData eventData)
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            CubeInteractions ci = hit.collider.GetComponent<CubeInteractions>();
            if (ci != null)
            {
                bool isChecked = false;
                foreach (int activator in data.controls[controlID].activators)
                {
                    if (activator == ci.cubeID)
                    {
                        isChecked = true;
                        //we have a match!
                        allCubes.AllRightAnswers(data.controls[controlID].activators);
                        foreach (int a in data.controls[controlID].activators)
                        {
                            data.currentCubeStatus[a] = true;
                        }
                        feedbackDisplay.ShowAnswer(true,controlID);
                        _panelImage.DOColor(completedColor, 0.5f).SetEase(Ease.InOutQuad);
                        _isDraggable = false;
                        _rect.DOScale(0, 0.5f);
                        //_rect.DOAnchorPos( _startRectPosition, 1).SetEase(Ease.InOutQuad).OnComplete(ResetLabel);
                    }
                }

                if (!isChecked)
                {
                    ci.WrongAnswer();
                    feedbackDisplay.ShowAnswer(false,controlID);
                    
                    //_rect.DOAnchorPos( _startRectPosition, 1).SetEase(Ease.InOutQuad).OnComplete(ResetLabel);
                }
            }
        }
        
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (_canHover)
        {
            StartCoroutine(CoolDownTime());
        }
        
    }

    IEnumerator CoolDownTime()
    {
        yield return new WaitForSeconds(1);
        string controlName = data.controls[controlID].name;
        string description = data.controls[controlID].description;
        //string references="";
        /*if (!_isDraggable)
        {
            foreach (var reference in data.controls[controlID].refLabels)
            {
                references += reference + ", ";
            }

            char[] c = {' ', ',' };
            references = references.Trim(c);
        }*/
        
        
        descriptionPanel.Appear(false, controlName, description);
}

    public void OnPointerExit(PointerEventData eventData)
    {
        if (_canHover)
        {
            StopAllCoroutines();
            descriptionPanel.Disappear();
        }
        
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (_isDraggable)
        {
            descriptionPanel.Disappear();
            _canHover = false;
            _dControl.canHover = false;
        }
        
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        _dControl.canHover = true;
        if (_isDraggable)
        {
            _rect.DOAnchorPos( _startRectPosition, 1).SetEase(Ease.InOutQuad).OnComplete(ResetLabel);
        }
    }

    void ResetLabel()
    {
        _canHover = true;
        
    }

    
}
   


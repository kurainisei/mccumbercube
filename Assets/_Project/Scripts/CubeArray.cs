﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CubeArray : MonoBehaviour
{
    public CubeInteractions[] cubes;
    public UnityEvent<int> OnRightCubeHover;
    public UnityEvent OnEndHover;
    // Start is called before the first frame update
    void Start()
    {
        cubes = GetComponentsInChildren<CubeInteractions>();
        foreach (CubeInteractions cube in cubes)
        {
            cube.cubeID = Array.IndexOf(cubes, cube);
        }
    }

    public void AllRightAnswers(int[] activators)
    {
        foreach (CubeInteractions cube in cubes)
        {
            foreach (int a in activators)
            {
                if (cube.cubeID == a)
                {
                    cube.RightAnswer();
                }
            }
        }
    }

    public void ResetAll()
    {
        foreach (CubeInteractions cube in cubes)
        {
            cube.Reset();
        }
    }

    public void HoveredOnRightCube(int i)
    {
        OnRightCubeHover.Invoke(i);
    }

    public void EndHover()
    {
        OnEndHover.Invoke();
    }
}

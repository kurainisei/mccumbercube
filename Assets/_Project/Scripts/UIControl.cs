﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class UIControl : MonoBehaviour
{
    private Canvas _canvas;

    public CanvasGroup fadingUI;
    // Start is called before the first frame update
    void Start()
    {
        _canvas = GetComponent<Canvas>();
        fadingUI.alpha = 0;
        _canvas.enabled = false;
    }

    public void FadeIn()
    {
        _canvas.enabled = true;
        fadingUI.DOFade(1, 1).SetEase(Ease.InOutQuad);
    }

    public void FadeOut()
    {
        fadingUI.DOFade(0, 1).SetEase(Ease.InOutQuad).OnComplete(CloseCanvas);
    }

    void CloseCanvas()
    {
        _canvas.enabled = false;
    }
}

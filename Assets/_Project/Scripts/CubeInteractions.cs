﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Assertions.Must;
using UnityEngine.Serialization;

public class CubeInteractions : MonoBehaviour
{
    public Color normalColor = Color.white;
    [FormerlySerializedAs("hoverColor")] public Color normalHoverColor = Color.yellow;
    public Color rightColor = Color.green;
    public Color rightHoverColor = Color.cyan;
    public Color wrongColor = Color.red;
    public int cubeID;
    private CubeArray _allCubes;
    private Renderer _r;
    private bool _isHovering;
    private bool _isHoverable = true;
    private Color _currentHoverColor;
    private Color _currentNormalColor;
    // Start is called before the first frame update
    void Start()
    {
        _r = GetComponent<Renderer>();
        _r.material.color = normalColor;
        _allCubes = GetComponentInParent<CubeArray>();
        _currentHoverColor = normalHoverColor;
        _currentNormalColor = normalColor;
    }

    private void OnEnable()
    {
        _isHoverable = true;
        _isHovering = false;
    }

    public void ToggleColor()
    {
        if (_isHoverable)
        {
            if (!_isHovering)
            {
                _isHovering = true;
                //transition to hover color
                _r.material.DOColor(_currentHoverColor,.5f).SetEase(Ease.InOutQuad);
                //if I'm hovering a right answer then I call the cubearray to deal with it.
                if (_currentHoverColor == rightHoverColor)
                {
                    _allCubes.HoveredOnRightCube(cubeID);
                }
            }
            else
            {
                _isHovering = false;
                //transition to normal color
                _r.material.DOColor(_currentNormalColor,.5f).SetEase(Ease.InOutQuad);
                //if I'm hovering a right answer then I call the cubearray to deal with it.
                if (_currentHoverColor == rightHoverColor)
                {
                    _allCubes.EndHover();
                }
            }
        }
    }

    public void RightAnswer()
    {
        _r.material.DOColor(rightColor, .5f).SetEase(Ease.InOutQuad);
        //_isHoverable=false;
        _currentHoverColor = rightHoverColor;
        _currentNormalColor = rightColor;
    }

    public void WrongAnswer()
    {
        _r.material.DOColor(wrongColor, .5f).SetEase(Ease.InOutQuad);
        _isHoverable = true;
    }

    public void Select()
    {
        _r.material.DOColor(Color.white, 0.5f).SetEase(Ease.InOutQuad);
        _isHoverable = false;
    }

    public void Deselect()
    {
        _r.material.color=normalColor;
        _isHoverable = true;
        _isHovering = false;
    }
    
    public void OnMouseEnter()
    {
        ToggleColor();
    }

    public void OnMouseExit()
    {
        ToggleColor();
    }

    public void Reset()
    {
        _r.material.color = normalColor;
        _currentNormalColor = normalColor;
        _currentHoverColor = normalHoverColor;
        _isHoverable = true;
        _isHovering = false;
    }
}
